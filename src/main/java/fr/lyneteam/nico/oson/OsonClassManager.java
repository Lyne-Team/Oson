package fr.lyneteam.nico.oson;

public interface OsonClassManager {
	public Class<?> getClass(String name);
}