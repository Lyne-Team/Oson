package fr.lyneteam.nico.oson;

public class Oson {
	public static OsonClassManager manager;
	
	public static Class<?> getClass(String name) {
		return manager.getClass(name);
	}
	
	public static void main(String[] arguements) {
		OsonObject firstOsonObject = new OsonObject(2, "test");
		System.out.println(firstOsonObject.getArgument1() + " " + firstOsonObject.getArgument2());
		Oson firstOson = new Oson(firstOsonObject);
		String json = firstOson.objectToJson();
		System.out.println(json);
		Oson secondOson = new Oson(json);
		OsonObject secondOsonObject = (OsonObject) secondOson.jsonToObject();
		System.out.println(secondOsonObject.getArgument1() + " " + secondOsonObject.getArgument2());
	}
	
	private final Object object;
	
	public Oson(Object object) {
		this.object = object;
	}
	
	public final Object getObject() {
		return this.object;
	}
	
	public final Object jsonToObject() {
		JsonObject object = (JsonObject) new JsonParser().parse(this.object.toString());
		return object.getObject();
	}
	
	public final String objectToJson() {
		JsonObject object = new JsonObject();
		object.addProperty("class", this.object.getClass().getName());
		object.addProperty("value", this.object);
		return object.toString();
	}
}