/*
 * Copyright (C) 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.lyneteam.nico.oson;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A class representing an object type in Json. An object consists of name-value
 * pairs where names are strings, and values are any other type of
 * {@link JsonElement}. This allows for a creating a tree of JsonElements. The
 * member elements of this object are maintained in order they were added.
 * 
 * Modified by Nicolas Demailly
 *
 * @author Inderjeet Singh
 * @author Joel Leitch
 * @author Nicolas Demailly
 */
public final class JsonObject extends JsonElement {
	private final LinkedTreeMap<String, JsonElement> members = new LinkedTreeMap<String, JsonElement>();

	@Override
	JsonObject deepCopy() {
		JsonObject result = new JsonObject();
		for (Map.Entry<String, JsonElement> entry : members.entrySet()) {
			result.add(entry.getKey(), entry.getValue().deepCopy());
		}
		return result;
	}

	/**
	 * Adds a member, which is a name-value pair, to self. The name must be a
	 * String, but the value can be an arbitrary JsonElement, thereby allowing
	 * you to build a full tree of JsonElements rooted at this node.
	 *
	 * @param property
	 *            name of the member.
	 * @param value
	 *            the member object.
	 */
	public void add(String property, JsonElement value) {
		if (value == null) {
			value = JsonNull.INSTANCE;
		}
		members.put(property, value);
	}

	/**
	 * Removes the {@code property} from this {@link JsonObject}.
	 *
	 * @param property
	 *            name of the member that should be removed.
	 * @return the {@link JsonElement} object that is being removed.
	 * @since 1.3
	 */
	public JsonElement remove(String property) {
		return members.remove(property);
	}

	public void addProperty(String property, Object value) {
		if (property == null || value == null) return;
		if (value instanceof Boolean) {
			addProperty(property, (Boolean) value);
		} else if (value instanceof Character) {
			addProperty(property, (Character) value);
		} else if (value instanceof ArrayList || value instanceof List) {
			JsonArray array = new JsonArray();
			for (Object object : (List<?>) value) {
				JsonObject add = new JsonObject();
				add.addProperty("class", object.getClass().getCanonicalName());
				add.addProperty("value", object);
				array.add(add);
			}
			add(property, array);
		} else if (value instanceof Number) {
			addProperty(property, (Number) value);
		} else if (value instanceof String) {
			addProperty(property, (String) value);
		} else {
			add(property, (JsonArray) this.getObject(value));
		}
	}
	
	private Object getObject(Object object) {
		JsonArray array = new JsonArray();		
		for (Field field : object.getClass().getDeclaredFields()) {
			if (!Modifier.isStatic(field.getModifiers()) && Modifier.isPrivate(field.getModifiers())) {
				field.setAccessible(true);
				try {
					JsonObject member = new JsonObject();
					member.addProperty("field", field.getName());
					member.addProperty("class", field.getType().getCanonicalName());
					member.addProperty("value", field.get(object));
					array.add(member);
				} catch (Exception exception) {
					// Ignore
				}
			}
		}
		return array;
	}

	/**
	 * Convenience method to add a primitive member. The specified value is
	 * converted to a JsonPrimitive of String.
	 *
	 * @param property
	 *            name of the member.
	 * @param value
	 *            the string value associated with the member.
	 */
	public void addProperty(String property, String value) {
		add(property, createJsonElement(value));
	}

	/**
	 * Convenience method to add a primitive member. The specified value is
	 * converted to a JsonPrimitive of Number.
	 *
	 * @param property
	 *            name of the member.
	 * @param value
	 *            the number value associated with the member.
	 */
	public void addProperty(String property, Number value) {
		add(property, createJsonElement(value));
	}

	public void addProperty(String property, List<String> value) {
		add(property, createJsonElement(value.toArray().toString()));
	}

	/**
	 * Convenience method to add a boolean member. The specified value is
	 * converted to a JsonPrimitive of Boolean.
	 *
	 * @param property
	 *            name of the member.
	 * @param value
	 *            the number value associated with the member.
	 */
	public void addProperty(String property, Boolean value) {
		add(property, createJsonElement(value));
	}

	/**
	 * Convenience method to add a char member. The specified value is converted
	 * to a JsonPrimitive of Character.
	 *
	 * @param property
	 *            name of the member.
	 * @param value
	 *            the number value associated with the member.
	 */
	public void addProperty(String property, Character value) {
		add(property, createJsonElement(value));
	}

	/**
	 * Creates the proper {@link JsonElement} object from the given
	 * {@code value} object.
	 *
	 * @param value
	 *            the object to generate the {@link JsonElement} for
	 * @return a {@link JsonPrimitive} if the {@code value} is not null,
	 *         otherwise a {@link JsonNull}
	 */
	private JsonElement createJsonElement(Object value) {
		return value == null ? JsonNull.INSTANCE : new JsonPrimitive(value);
	}

	/**
	 * Returns a set of members of this object. The set is ordered, and the
	 * order is in which the elements were added.
	 *
	 * @return a set of members of this object.
	 */
	public Set<Map.Entry<String, JsonElement>> entrySet() {
		return members.entrySet();
	}

	/**
	 * Convenience method to check if a member with the specified name is
	 * present in this object.
	 *
	 * @param memberName
	 *            name of the member that is being checked for presence.
	 * @return true if there is a member with the specified name, false
	 *         otherwise.
	 */
	public boolean has(String memberName) {
		return members.containsKey(memberName);
	}

	/**
	 * Returns the member with the specified name.
	 *
	 * @param memberName
	 *            name of the member that is being requested.
	 * @return the member matching the name. Null if no such member exists.
	 */
	public JsonElement get(String memberName) {
		return members.get(memberName);
	}

	/**
	 * Convenience method to get the specified member as a JsonPrimitive
	 * element.
	 *
	 * @param memberName
	 *            name of the member being requested.
	 * @return the JsonPrimitive corresponding to the specified member.
	 */
	public JsonPrimitive getAsJsonPrimitive(String memberName) {
		return (JsonPrimitive) members.get(memberName);
	}

	/**
	 * Convenience method to get the specified member as a JsonArray.
	 *
	 * @param memberName
	 *            name of the member being requested.
	 * @return the JsonArray corresponding to the specified member.
	 */
	public JsonArray getAsJsonArray(String memberName) {
		return (JsonArray) members.get(memberName);
	}

	/**
	 * Convenience method to get the specified member as a JsonObject.
	 *
	 * @param memberName
	 *            name of the member being requested.
	 * @return the JsonObject corresponding to the specified member.
	 */
	public JsonObject getAsJsonObject(String memberName) {
		return (JsonObject) members.get(memberName);
	}

	@Override
	public boolean equals(Object o) {
		return (o == this) || (o instanceof JsonObject && ((JsonObject) o).members.equals(members));
	}

	@Override
	public int hashCode() {
		return members.hashCode();
	}

	public Object getObject() {
		return this.getObject(this, "value", this.get("class").getAsString());
	}
	
	public Object getObject(JsonObject object, String name, String clazz) {
		try {
			if (!object.has(name)) return null;
			Class<?> objectClass = Oson.getClass(clazz);
			if (Boolean.class.isAssignableFrom(objectClass)) {
				return object.get(name).getAsBoolean();
			} else if (Character.class.isAssignableFrom(objectClass)) {
				return object.get(name).getAsCharacter();
			} else if (List.class.isAssignableFrom(objectClass)) {
				JsonArray array = object.get(name).getAsJsonArray();
				List<Object> list = new ArrayList<Object>();
				for (JsonElement element : array) list.add(this.getObject((JsonObject) element, "value", ((JsonObject) element).get("class").getAsString()));
				return list;
			} else if (Double.class.isAssignableFrom(objectClass)) {
				return object.get(name).getAsDouble();
			} else if (Float.class.isAssignableFrom(objectClass)) {
				return object.get(name).getAsFloat();
			} else if (Integer.class.isAssignableFrom(objectClass)) {
				return object.get(name).getAsInt();
			} else if (Long.class.isAssignableFrom(objectClass)) {
				return object.get(name).getAsLong();
			} else if (Number.class.isAssignableFrom(objectClass)) {
				return object.get(name).getAsNumber();
			} else if (String.class.isAssignableFrom(objectClass)) {
				return object.get(name).getAsString();
			} else {
				Object objectInstance = objectClass.getConstructor().newInstance();
				for (JsonElement temporaly : object.get(name).getAsJsonArray()) {
					JsonObject member = (JsonObject) temporaly;
					String field = member.get("field").getAsString();
					Field memberField = objectClass.getDeclaredField(field);
					memberField.setAccessible(true);
					memberField.set(objectInstance, this.getObject(member, "value", member.get("class").getAsString()));
				}
				return objectInstance;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}
}