package fr.lyneteam.nico.oson;

public class OsonObject {
	private Integer argument1;
	private String argument2;

	public OsonObject(int argument1, String argument2) {
		this.argument1 = argument1;
		this.argument2 = argument2;
	}
	
	public OsonObject() {
		// Used for remake an object
	}

	public final Integer getArgument1() {
		return argument1;
	}

	public void setArgument1(Integer argument1) {
		this.argument1 = argument1;
	}

	public final String getArgument2() {
		return argument2;
	}

	public void setArgument2(String argument2) {
		this.argument2 = argument2;
	}
}