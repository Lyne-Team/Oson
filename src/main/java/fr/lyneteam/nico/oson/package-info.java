/**
 * This package provides the {@link fr.lyneteam.nico.overman.gson.Gson} class to convert Json to Java and
 * vice-versa.
 *
 * <p>The primary class to use is {@link fr.lyneteam.nico.overman.gson.Gson} which can be constructed with
 * {@code new Gson()} (using default settings) or by using {@link fr.lyneteam.nico.overman.gson.GsonBuilder}
 * (to configure various options such as using versioning and so on).</p>
 *
 * @author Inderjeet Singh, Joel Leitch
 */
package fr.lyneteam.nico.oson;